StockX Coding Challenge:

1. List posts (title, image, author, subreddit, time, number of comments) for a given subreddit.
2.  Allow user to change subreddits.
3.  View a clicked post in a WebView.

This is built in Kotlin using the MVVM design pattern.

I've also completed this coding challenge, in the past, in Java using the MVP design pattern.  That repo is located [here](https://bitbucket.org/aelaster/stockx/src/master/).