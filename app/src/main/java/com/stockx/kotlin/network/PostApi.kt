package com.stockx.kotlin.network

import com.stockx.kotlin.model.RedditResponse
import com.stockx.kotlin.utils.DEFAULT_POST_COUNT
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

/**
 * The interface which provides methods to get result of webservices
 */
interface PostApi {
    /**
     * Get the list of the posts from the API
     * @param subreddit Name of subreddit to load
     * @param after Tells reddit to return posts after the previously loaded posts
     * @param limit Number of posts to load
     */
    @GET("{subreddit}/.json")
    fun getPosts(@Path("subreddit") subreddit: String,
                 @Query("after") after: String = "",
                 @Query("limit") limit: String = DEFAULT_POST_COUNT): Observable<RedditResponse>
}