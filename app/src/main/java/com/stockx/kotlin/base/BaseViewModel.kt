package com.stockx.kotlin.base

import android.arch.lifecycle.ViewModel
import com.stockx.kotlin.injection.component.DaggerViewModelInjector
import com.stockx.kotlin.injection.component.ViewModelInjector
import com.stockx.kotlin.injection.module.NetworkModule
import com.stockx.kotlin.ui.post.PostListViewModel

abstract class BaseViewModel: ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
        .builder()
        .networkModule(NetworkModule)
        .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PostListViewModel -> injector.inject(this)
        }
    }
}