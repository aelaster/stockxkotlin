package com.stockx.kotlin.utils

/** The base URL of the API */
const val BASE_URL: String = "https://www.reddit.com/r/"
const val DEFAULT_SUBREDDIT: String = "all"
const val DEFAULT_POST_COUNT: String = "20"