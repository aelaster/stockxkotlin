package com.stockx.kotlin.model

data class RedditResponse(val data: RedditDataResponse)

data class RedditDataResponse(
    val children: List<RedditChildrenResponse>,
    val after: String?
)

data class RedditChildrenResponse(val data: RedditDataPosts)

/**
 * Class which provides a model for post
 * @constructor Sets all properties of the post
 * @property author the username of the author of the post
 * @property title the title of the post
 * @property num_comments the number of comments on the post
 * @property created the unix timestamp of the creation date of the post
 * @property thumbnail the url of the thumbnail, if one exists, of the post
 * @property url the url of the content of the post
 * @property subreddit the name of the subreddit of the post
 */
data class RedditDataPosts(
    val author: String,
    val title: String,
    val num_comments: Int,
    val created: Long,
    val thumbnail: String,
    val url: String,
    val subreddit: String
)