package com.stockx.kotlin.ui.post

import android.arch.lifecycle.MutableLiveData
import com.stockx.kotlin.base.BaseViewModel
import com.stockx.kotlin.model.RedditChildrenResponse
import com.stockx.kotlin.utils.extension.getFriendlyTime

class PostViewModel: BaseViewModel() {
    private val postTitle = MutableLiveData<String>()
    private val postBody = MutableLiveData<String>()
    private val postThumb = MutableLiveData<String>()
    private val postURL = MutableLiveData<String>()
    private val postComments = MutableLiveData<String>()
    private val postTime = MutableLiveData<String>()
    private val postSubreddit = MutableLiveData<String>()

    fun bind(post: RedditChildrenResponse) {
        postTitle.value = post.data.title
        postBody.value = post.data.author
        postThumb.value = post.data.thumbnail
        postURL.value = post.data.url
        postComments.value = post.data.num_comments.toString() + " comments"
        postTime.value = post.data.created.getFriendlyTime()
        postSubreddit.value = "/r/" + post.data.subreddit
    }

    fun getPostTitle(): MutableLiveData<String> {
        return postTitle
    }

    fun getPostBody(): MutableLiveData<String> {
        return postBody
    }

    fun getPostThumb(): MutableLiveData<String> {
        return postThumb
    }

    fun getPostURL(): MutableLiveData<String> {
        return postURL
    }

    fun getPostComments(): MutableLiveData<String> {
        return postComments
    }

    fun getPostTime(): MutableLiveData<String> {
        return postTime
    }

    fun getPostSubreddit(): MutableLiveData<String> {
        return postSubreddit
    }
}