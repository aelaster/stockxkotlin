package com.stockx.kotlin.ui.post

import android.content.Intent
import android.databinding.DataBindingUtil
import android.support.v4.content.ContextCompat.startActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.stockx.kotlin.R
import com.stockx.kotlin.databinding.ItemPostBinding
import com.stockx.kotlin.model.RedditChildrenResponse
import com.stockx.kotlin.ui.webview.WebViewActivity

class PostListAdapter: RecyclerView.Adapter<PostListAdapter.ViewHolder>() {

    private var postList:ArrayList<RedditChildrenResponse> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_post, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    override fun getItemCount(): Int {
        return postList.size
    }

    /**
     * Add new posts to the list.
     * This will be used both on a new list and when the infinite scroller retrieves the next set of records.
     * @param postList List of posts retrieved from Reddit.
     */
    fun updatePostList(postList:List<RedditChildrenResponse>){
        this.postList.addAll(postList)
        notifyDataSetChanged()
    }

    /**
     * Clear the existing list when switching subreddits.
     */
    fun clearPostList(){
        this.postList.clear()
        notifyDataSetChanged()
    }

    inner class ViewHolder(private val binding: ItemPostBinding):RecyclerView.ViewHolder(binding.root), View.OnClickListener {
        init {
            binding.root.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val intent = Intent(v!!.context, WebViewActivity::class.java)
            intent.putExtra("url", viewModel.getPostURL().value)
            startActivity(v.context, intent, null)
        }

        private val viewModel = PostViewModel()

        fun bind(post:RedditChildrenResponse){
            viewModel.bind(post)
            binding.viewModel = viewModel
        }
    }
}