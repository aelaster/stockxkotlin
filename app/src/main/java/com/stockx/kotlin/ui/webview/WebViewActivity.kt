package com.stockx.kotlin.ui.webview

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.webkit.WebView
import android.webkit.WebViewClient
import com.stockx.kotlin.R

class WebViewActivity : AppCompatActivity() {
    private var mywebview: WebView? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_webview)
        mywebview = findViewById(R.id.webview)
        mywebview!!.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(view: WebView?, url: String?): Boolean {
                view?.loadUrl(url)
                return true
            }
        }
        mywebview!!.settings.useWideViewPort = true
        mywebview!!.settings.loadWithOverviewMode = true
        mywebview!!.loadUrl(intent.getStringExtra("url"))
    }
}