package com.stockx.kotlin.ui.post

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.annotation.StringRes
import android.support.design.widget.Snackbar
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.EditText
import com.stockx.kotlin.R
import com.stockx.kotlin.databinding.ActivityPostListBinding
import com.stockx.kotlin.utils.InfiniteScrollListener

class PostListActivity: AppCompatActivity() {
    private lateinit var binding: ActivityPostListBinding
    private lateinit var viewModel: PostListViewModel

    private var errorSnackbar: Snackbar? = null

    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_list)
        binding.postList.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
        binding.postList.addOnScrollListener(InfiniteScrollListener({ viewModel.loadPosts(viewModel.subreddit) },
            binding.postList.layoutManager as LinearLayoutManager
        ))
        binding.fab.setOnClickListener {
            showDialog()
        }

        viewModel = ViewModelProviders.of(this).get(PostListViewModel::class.java)
        viewModel.errorMessage.observe(this, Observer {
                errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })

        binding.viewModel = viewModel
    }

    private fun showDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.subreddit_title)
        val dialogLayout = View.inflate(this, R.layout.dialog_subreddit, null)
        val editText = dialogLayout.findViewById<EditText>(R.id.subreddit)
        builder.setView(dialogLayout)
        builder.setPositiveButton(R.string.subreddit_submit) {
            _, _ -> viewModel.loadNewSubreddit(editText.text.toString())
        }
        builder.show()
    }


    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_LONG)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }
}