package com.stockx.kotlin.ui.post

import android.arch.lifecycle.MutableLiveData
import android.view.View
import com.stockx.kotlin.R
import com.stockx.kotlin.base.BaseViewModel
import com.stockx.kotlin.model.RedditDataResponse
import com.stockx.kotlin.network.PostApi
import com.stockx.kotlin.utils.DEFAULT_SUBREDDIT
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PostListViewModel: BaseViewModel(){
    @Inject
    lateinit var postApi: PostApi

    val postListAdapter: PostListAdapter = PostListAdapter()

    private lateinit var subscription: Disposable
    private var after: String = ""
    var subreddit: String = DEFAULT_SUBREDDIT

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val errorMessage: MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts(subreddit) }

    init{
        loadPosts(DEFAULT_SUBREDDIT)
    }

    /**
     * Loading a new subreddit.
     * This will clear the existing list and the value of after, indicating a fresh list.
     * @param subreddit Subreddit to load
     */
    fun loadNewSubreddit(subreddit: String){
        after = ""
        postListAdapter.clearPostList()
        loadPosts(subreddit)
    }

    /**
     * Retrieving posts from Reddit.
     * After is used for the infinite scroller
     * @param subreddit Subreddit to load
     */
    fun loadPosts(subreddit: String){
        this.subreddit = subreddit
        subscription = postApi.getPosts(subreddit, after)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { onRetrievePostListStart() }
            .doOnTerminate { onRetrievePostListFinish() }
            .subscribe(
                { result -> onRetrievePostListSuccess(result.data) },
                { onRetrievePostListError() }
            )
    }

    private fun onRetrievePostListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(response:RedditDataResponse){
        after = response.after.orEmpty()  // after variable to be used for the infinite scroller
        postListAdapter.updatePostList(response.children)
    }

    private fun onRetrievePostListError(){
        errorMessage.value = R.string.post_error
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}